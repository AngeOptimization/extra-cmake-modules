Ange copy of Extra CMake Modules
================================

Copy of Extra CMake Modules from
https://projects.kde.org/projects/kdesupport/extra-cmake-modules with no
modifications.

Used as part of the stow project in order to make the build on MS-Windows and
Debian easier.

The upstream readme file is README.rst.

Upstream repository: git://anongit.kde.org/extra-cmake-modules

Release
=======

Base the version number on the upstream release version.

Example of version: 1.4.0-ange.1
